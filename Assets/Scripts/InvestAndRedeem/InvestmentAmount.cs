﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class InvestmentAmount : AmountSelector {

	private CashText cashText;
	private InvestmentProduct product;
	private InvestmentList investmentList;

	// Use this for initialization
	protected override void Start () {
		cashText = GameObject.FindObjectOfType<CashText> ();
		product = this.transform.parent.parent.GetComponent<InvestmentProduct> ();
		investmentList = this.transform.root.Find ("GamePanel/InvestmentList").GetComponent<InvestmentList> ();

		base.Start ();
	}

	public void InvestAmount () {
		if (amount == 0) {
			return;
		}

		amount = Math.Min (amount, GameData.cash);
		bool success = GameData.investments.Invest (product.type, amount);

		if (success) {
			cashText.DecreaseCash (amount);
			investmentList.UpdateList ();

			double investAmount = (double)amount;
			double profit = (double)GameData.investmentStats [product.type].yearProfit;
			double factor = GameData.InvestExperienceFactor;
			GameData.investExp += investAmount * profit * factor;
			GameData.maxInvestExp = Math.Max (GameData.investExp, GameData.maxInvestExp);
		}
	}

	protected override long MinValue () {
		return GameData.investmentStats [product.type].minAmount;
	}

	protected override long MaxValue () {
		return GameData.cash;
	}
}
