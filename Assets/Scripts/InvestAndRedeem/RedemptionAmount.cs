﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class RedemptionAmount : AmountSelector {

	private CashText cashText;
	private Investment investment;
	private InvestmentList investmentList;

	// Use this for initialization
	protected override void Start () {
		cashText = GameObject.FindObjectOfType<CashText> ();
		investment = this.transform.parent.parent.GetComponent<Investment> ();
		investmentList = this.transform.root.Find ("GamePanel/InvestmentList").GetComponent<InvestmentList> ();

		base.Start ();
	}

	public void RedeemAmount () {
		if (amount == 0) {
			return;
		}

		long redemptionAmount = GameData.investments.Redeem (investment.type, amount);

		if (redemptionAmount != 0) {
			cashText.IncreaseCash (redemptionAmount);
			investmentList.UpdateList ();

			double investAmount = (double)redemptionAmount;
			double profit = (double)GameData.investmentStats [investment.type].yearProfit;
			double factor = GameData.InvestExperienceFactor;
			GameData.investExp -= investAmount * profit * factor;
		}
	}

	protected override long MinValue () {
		return 1;
	}

	protected override long MaxValue ()
	{
		return investment.GetAvailableAmount ();
	}
}
