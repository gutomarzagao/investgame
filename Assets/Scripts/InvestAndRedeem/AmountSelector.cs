﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using System.Threading;
using System.Collections;

public abstract class AmountSelector : MonoBehaviour
{

	private const int SliderGranularity = 20;

	private bool componentLoaded = false;
	protected long amount;

	private Slider amountSlider;
	private Text text;
	private Button okButton;

	// Use this for initialization
	protected virtual void Start ()
	{
		amountSlider = this.transform.parent.Find ("AmountSlider").GetComponent<Slider> ();
		text = this.GetComponent<Text> ();
		okButton = this.transform.parent.Find ("OkButton").GetComponent<Button> ();

		componentLoaded = true;
		ChangeAmount ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (amountSlider.value == amountSlider.maxValue) {
			amount = long.MaxValue;
			UpdateText (MaxValue ());
		}
	}

	void OnEnable ()
	{
		if (componentLoaded) {
			ChangeAmount ();
		}
	}

	public void ChangeAmount ()
	{
		long min = MinValue ();
		long max = MaxValue ();

		long precision = CalculatePrecision (max);
		long adustedMax = (((max - 1) / precision) + 1) * precision;

		var targetValue = Math.Round (adustedMax * amountSlider.value);
		amount = ((long)targetValue + precision / 2) / precision * precision;

		amountSlider.value = (float)amount / adustedMax;

		if (amountSlider.value == amountSlider.maxValue) {
			amount = long.MaxValue;
			UpdateText (MaxValue ());
		} else {
			UpdateText (amount);
		}

		bool sliderInMinValue = amountSlider.value == amountSlider.minValue;
		bool sliderInMaxValue = amountSlider.value == amountSlider.maxValue;
		bool amountInsideRange = min <= amount && amount <= max;
		okButton.interactable = sliderInMinValue || sliderInMaxValue || amountInsideRange;
	}

	private long CalculatePrecision (long max)
	{
		long precision = max / SliderGranularity;
		StringBuilder precisionString = new StringBuilder (precision.ToString ());

		char firstDigit = precisionString [0];
		do {
			if (firstDigit == '9') {
				firstDigit = '0';
				precisionString.Append ('0');
			} else {
				firstDigit++;
			}
		} while (firstDigit != '1' && firstDigit != '2' && firstDigit != '5');

		precisionString [0] = firstDigit;
		for (int i = 1; i < precisionString.Length; i++) {
			precisionString [i] = '0';
		}

		return long.Parse (precisionString.ToString ());
	}

	protected abstract long MinValue ();

	protected abstract long MaxValue ();

	private void UpdateText (long value)
	{
		text.text = "$" + value.ToString (GameData.numberFormat, GameData.formatProvider);
	}
}
