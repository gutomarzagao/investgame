﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour {

	public ScrollRect scrollPanel;
	public MenuBtn[] menuOptions;
	public MenuBtn activeMenu;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < menuOptions.Length; i++) {
			Deactivate (menuOptions[i]);
		}

		Activate (activeMenu);
	}

	public void SelectMenuOption (MenuBtn option) {
		if (activeMenu == option) {
			return;
		}

		Deactivate (activeMenu);
		Activate (option);
	}

	private void Activate (MenuBtn option) {
		// Show objects
		activeMenu = option;
		option.panel.SetActive (true);
		option.GetComponent<RawImage> ().enabled = true;

		// Set scrollable content
		scrollPanel.content = option.panel.GetComponent<RectTransform> ();

		// Reset initial position
		Transform transform = option.panel.transform;
		transform.localPosition = new Vector3 (transform.localPosition.x, 0, transform.localPosition.z);

		// Set all investments to view state
		InvestmentProductList list = option.panel.GetComponent<InvestmentProductList> ();
		if (list != null) {
			list.SetState (true);
		}
	}

	private void Deactivate (MenuBtn option) {
		// Hide objects
		option.panel.SetActive (false);
		option.GetComponent<RawImage> ().enabled = false;
	}
}
