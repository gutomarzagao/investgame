﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public abstract class InvestmentProduct : MonoBehaviour {
	
	public InvestmentType type;
	private InvestmentProductList investmentList;

	private GameObject investmentInfo;
	private GameObject investmentSelection;
	private GameObject investmentIcon;

	protected Button actionButton;

	// Use this for initialization
	protected virtual void Start () {
		investmentList = this.transform.parent.GetComponent<InvestmentProductList> ();
		investmentInfo = this.transform.Find ("InvestmentInfo").gameObject;
		investmentSelection = this.transform.Find ("InvestmentSelection").gameObject;
		investmentIcon = this.transform.Find ("InvestmentInfo/InvestmentIcon").gameObject;
		actionButton = investmentInfo.GetComponent<Button> ();

		investmentInfo.SetActive (true);
		investmentSelection.SetActive (false);

		Sprite investmentSprite = Resources.Load <Sprite> ("InvestimentsIcons/" + type.ToString());
		if (investmentSprite == null) {
			Debug.LogError ("Missing investment icon: " + type.ToString ());
		}

		investmentIcon.GetComponent<Image> ().sprite = investmentSprite;
	}

	public void ToggleState () {
		investmentInfo.SetActive (!investmentInfo.activeSelf);
		investmentSelection.SetActive (!investmentSelection.activeSelf);

		if (investmentSelection.activeSelf) {
			investmentList.SetState (true, this);
		}
	}

	public void SetState (bool view) {
		if (investmentInfo != null && investmentSelection != null) {
			investmentInfo.SetActive (view);
			investmentSelection.SetActive (!view);
		}
	}
}
