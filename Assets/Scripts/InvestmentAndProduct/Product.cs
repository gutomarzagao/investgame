﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class Product : InvestmentProduct {
	
	private Text productName;
	private Text minAmount;
	private Text liquidity;
	private Text profitability;
	
	// Use this for initialization
	protected override void Start () {
		productName = this.transform.Find ("InvestmentInfo/InvestmentName").GetComponent<Text> ();
		minAmount = this.transform.Find ("InvestmentInfo/InvestmentMinimum").GetComponent<Text> ();
		liquidity = this.transform.Find ("InvestmentInfo/InvestmentLiquidity").GetComponent<Text> ();
		profitability = this.transform.Find ("InvestmentInfo/InvestmentProfitability").GetComponent<Text> ();

		if (Screen.width < 300) {
			SetFontSizes (12, 16);
		} else if (Screen.width < 400) {
			SetFontSizes (15, 22);
		} else if (Screen.width < 580) {
			SetFontSizes (20, 30);
		} else if (Screen.width < 840) {
			SetFontSizes (28, 42);
		} else if (Screen.width < 1200) {
			SetFontSizes (40, 60);
		} else {
			SetFontSizes (60, 90);
		}

		SetTextInfo ();
		base.Start ();
	}

	void Update() {
		InvestmentParam param = GameData.investmentStats [type];
		actionButton.interactable = GameData.cash >= param.minAmount && GameData.cash > 0;
	}

	private void SetFontSizes (int small, int large) {
		productName.fontSize = large;
		minAmount.fontSize = small;
		liquidity.fontSize = small;
		profitability.fontSize = large;
	}
	
	private void SetTextInfo () {
		InvestmentParam param = GameData.investmentStats [type];
		productName.text = param.name.ToUpper();
		profitability.text = String.Format ("{0}% ao ano", param.yearProfit);

		string min = param.minAmount.ToString (GameData.numberFormat, GameData.formatProvider);
		minAmount.text = String.Format ("Mínimo ${0}", min);

		if (param.minTimeSpan.Equals(TimeSpan.Zero)) {
			liquidity.text = String.Format ("Resgate imediato", param.minAmount);
		} else {
			liquidity.text = String.Format ("Resgate em {0}", param.redeemString);
		}
	}
}
