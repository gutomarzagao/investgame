﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class Investment : InvestmentProduct {

	private Text investmentName;
	private Text profit;
	private Text liquidity;

	private InvestmentPosition position;

	// Use this for initialization
	protected override void Start () {
		investmentName = this.transform.Find ("InvestmentInfo/InvestmentName").GetComponent<Text> ();
		profit = this.transform.Find ("InvestmentInfo/InvestmentProfit").GetComponent<Text> ();
		liquidity = this.transform.Find ("InvestmentInfo/InvestmentLiquidity").GetComponent<Text> ();

		if (Screen.width < 300) {
			SetFontSizes (12, 16);
		} else if (Screen.width < 400) {
			SetFontSizes (15, 22);
		} else if (Screen.width < 580) {
			SetFontSizes (20, 30);
		} else if (Screen.width < 840) {
			SetFontSizes (28, 42);
		} else if (Screen.width < 1200) {
			SetFontSizes (40, 60);
		} else {
			SetFontSizes (60, 90);
		}

		SetTextInfo ();
		base.Start ();
	}

	void Update () {
		UpdateInvestmentPosition ();
	}

	private void SetFontSizes (int small, int large) {
		investmentName.fontSize = large;
		profit.fontSize = large;
		liquidity.fontSize = small;
	}

	public long GetAvailableAmount () {
		return position.availableAmount;
	}

	private void UpdateInvestmentPosition () {
		position = GameData.investments.GetPosition (type);

		string totalAmount = position.totalAmount.ToString (GameData.numberFormat, GameData.formatProvider);
		profit.text = String.Format("${0}", totalAmount);

		if (position.availableAmount == position.totalAmount) {
			liquidity.text = "pode resgatar tudo";
			actionButton.interactable = true;
		} else if (position.availableAmount == 0) {
			liquidity.text = String.Format("não pode resgatar ainda");
			actionButton.interactable = false;
		} else {
			string availableAmount = position.availableAmount.ToString (GameData.numberFormat, GameData.formatProvider);
			liquidity.text = String.Format("pode resgatar ${0}", availableAmount);
			actionButton.interactable = true;
		}
	}

	private void SetTextInfo () {
		InvestmentParam param = GameData.investmentStats [type];
		investmentName.text = param.name.ToUpper();
	}
}
