﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class Ground : MonoBehaviour, ICanvasRaycastFilter {

	public int Index { get; set; }
	public CashText cash;

	private RectTransform rectTransform;
	private Image image;
	private Plantation plantation;

	// Use this for initialization
	void Awake () {
		rectTransform = this.GetComponent<RectTransform> ();
		image = this.GetComponent<Image> ();
		plantation = this.transform.parent.GetComponent<Plantation> ();
	}

	void OnEnable() {
		StartCoroutine (SetSprite ());
	}

	public void GroundClick ()
	{
		DateTime now = DateTime.Now;
		TimeSpan? elapsedTime = now - GameData.plantation [Index];

		if (elapsedTime == null) {
			GameData.plantation [Index] = now;
			plantation.AddCrop (now);
		} else if (elapsedTime >= GameData.plantationTime) {
			GameData.plantation [Index] = null;
			cash.IncreaseCash (GameData.plantationEarnings);
		}

		StartCoroutine (SetSprite ());
	}

	private IEnumerator SetSprite () {
		TimeSpan? elapsedTime = DateTime.Now - GameData.plantation [Index];

		if (elapsedTime == null) {
			image.sprite = plantation.groundSprite;
		} else if (elapsedTime < GameData.plantationTime) {
			image.sprite = plantation.seedSprite;

			float seconds = (float)(GameData.plantationTime - elapsedTime).Value.TotalSeconds;
			yield return new WaitForSeconds (seconds);
			image.sprite = plantation.plantationSprite;
		} else {
			image.sprite = plantation.plantationSprite;
		}
	}

	// Disable raycast target when mouse is over transparent area
	public bool IsRaycastLocationValid (Vector2 screenPosition, Camera eventCamera)
	{
		Vector2 localPos;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPosition, null, out localPos);

		float x = (localPos.x - rectTransform.rect.min.x) / rectTransform.rect.size.x;
		float y = (localPos.y - rectTransform.rect.min.y) / (rectTransform.rect.size.y * 83f / 99f);

		x = Math.Min (x, 1 - x);
		y = Math.Min (y, 1 - y);

		return x + y > 0.5f;
	}
}
