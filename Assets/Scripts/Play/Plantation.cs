﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class Plantation : MonoBehaviour
{

	public Text nextCropText;
	public Sprite groundSprite;
	public Sprite seedSprite;
	public Sprite plantationSprite;
	public Ground[] grounds;

	private List<DateTime> nextCrops = new List<DateTime> ();

	void Awake ()
	{
		if (GameData.plantation == null || grounds.Length != GameData.plantation.Length) {
			GameData.plantation = new DateTime?[grounds.Length];
		}

		InitializeGrounds ();
	}

	private void InitializeGrounds ()
	{
		for (int i = 0; i < grounds.Length; i++) {
			grounds [i].Index = i;
			AddCrop (GameData.plantation [i]);
		}
	}

	void Update ()
	{
		UpdateCropText ();
	}

	private void UpdateCropText ()
	{
		if (nextCrops.Count > 0) {
			TimeSpan nextCrop = nextCrops [0] - DateTime.Now;

			if (nextCrop.Ticks > 0) {
				nextCropText.text = "Próxima colheita\n";
				nextCropText.text += new DateTime (nextCrop.Ticks).ToString ("mm:ss");
			} else {
				nextCrops.RemoveAt (0);
			}
		} else {
			nextCropText.text = "";
		}
	}

	public void AddCrop (DateTime? plantationTime)
	{
		if (plantationTime.HasValue) {
			DateTime cropTime = plantationTime.Value.Add (GameData.plantationTime);
			nextCrops.Add (cropTime);
			nextCrops.Sort ();
		}
	}

}
