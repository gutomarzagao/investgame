﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExperienceBar : MonoBehaviour {

	void Update () {
		float experienceProgress = (float)GameData.experience / (float)DataLoader.GetLevelExperience (GameData.level);
		RectTransform transform = GetComponent<RectTransform> ();
		transform.anchorMax = new Vector2 (experienceProgress, transform.anchorMax.y);
	}
}
