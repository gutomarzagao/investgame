﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelText : MonoBehaviour {

	Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
	}

	void Update () {
		text.text = string.Format ("LV <b>{0}</b>", GameData.level);
	}
}
