﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BlockBanner : MonoBehaviour {

	private int unblockingLevel;

	// Use this for initialization
	void Start () {
		InvestmentType type = this.transform.parent.GetComponent<Product> ().type;
		unblockingLevel = GameData.investmentStats [type].minLevel;

		Text text = this.transform.Find ("UnblockText").GetComponent<Text> ();
		text.text = string.Format ("Desbloqueie no LV {0}", unblockingLevel);

		CheckLevel ();
	}
	
	// Update is called once per frame
	void Update () {
		CheckLevel ();
	}

	void CheckLevel() {
		if (GameData.level >= unblockingLevel) {
			Destroy (gameObject);
		}
	}
}
