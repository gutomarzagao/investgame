﻿using UnityEngine;
using System.Collections;

public class ProductList : InvestmentProductList {

	protected override bool ShouldInstantiate (InvestmentType type)
	{
		return true;
	}

	protected override string PrefabPath ()
	{
		return "Prefabs/Product";
	}
}
