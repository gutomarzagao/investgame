﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public abstract class InvestmentProductList : MonoBehaviour {

	private const float InvestmentAspectRatio = 3;

	private Dictionary<InvestmentType, GameObject> investments = new Dictionary<InvestmentType, GameObject> ();

	// Use this for initialization
	void Start () {
		UpdateList ();
	}

	public void UpdateList() {
		foreach (InvestmentType type in Enum.GetValues (typeof(InvestmentType))) {
			bool shouldInstantiate = ShouldInstantiate (type);
			bool instanceExists = investments.ContainsKey(type);

			if (shouldInstantiate && !instanceExists) {
				GameObject investment = Instantiate (Resources.Load(PrefabPath ())) as GameObject;
				investment.transform.SetParent (this.transform);

				InvestmentProduct script = investment.GetComponent<InvestmentProduct> ();
				script.type = type;

				investments.Add (type, investment);
			} else if (!shouldInstantiate && instanceExists) {
				GameObject investment = investments [type];
				Destroy (investment);
				investments.Remove (type);
			}
		}

		foreach (InvestmentType type in Enum.GetValues (typeof(InvestmentType))) {
			if (investments.ContainsKey (type)) {
				investments [type].transform.SetAsLastSibling ();
			}
		}

		AspectRatioFitter transform = GetComponent<AspectRatioFitter> ();
		transform.aspectRatio = InvestmentAspectRatio / investments.Count;
	}

	public void SetState (bool view, InvestmentProduct except = null) {
		InvestmentProduct[] investments = this.GetComponentsInChildren<InvestmentProduct> ();

		foreach (InvestmentProduct investment in investments) {
			if (except == null || investment != except) {
				investment.SetState (true);
			}
		}
	}

	public GameObject GetInvestment(InvestmentType type)
	{
		return investments [type];
	}


	protected abstract bool ShouldInstantiate (InvestmentType type);

	protected abstract string PrefabPath ();
}
