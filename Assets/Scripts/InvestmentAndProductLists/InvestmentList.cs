﻿using UnityEngine;
using System.Collections;

public class InvestmentList : InvestmentProductList {

	protected override bool ShouldInstantiate (InvestmentType type)
	{
		return GameData.investments.HasType (type);
	}

	protected override string PrefabPath ()
	{
		return "Prefabs/Investment";
	}
}
