﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;
using System.Collections;

public class Calendar : MonoBehaviour {

	public Text dateText;
	public Text timeText;

	// Update is called once per frame
	void Update () {
		UpdateDateTime ();
	}

	void UpdateDateTime () {
		TimeSpan elapsedTime = DateTime.Now - GameData.startingDate;

		long totalMinutes = (long)(elapsedTime.TotalMinutes * GameData.timeRate);
		long totalHours = (long)(elapsedTime.TotalHours * GameData.timeRate);
		long totalDays = (long)(elapsedTime.TotalDays * GameData.timeRate);

		int minute = (int)(totalMinutes % 60);
		int hour = (int)(totalHours % 24);

		int day = (int)(totalDays % 30) + 1;
		int month = (int)((totalDays / 30) % 12) + 1;
		int year = (int)(totalDays / 360) + 1;

		DateTime dateTime = new DateTime (year, month, day, hour, minute, 0);
		dateText.text = dateTime.ToString ("dd / MMM, ano y", GameData.formatProvider);
		timeText.text = dateTime.ToString ("HH:mm", GameData.formatProvider);
	}
}
