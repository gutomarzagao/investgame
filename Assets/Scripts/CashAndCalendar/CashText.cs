﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;

public class CashText : MonoBehaviour {

	private Text text;
	private Text dollarSignText;

	// Use this for initialization
	void Start () {
		text = this.GetComponent<Text> ();
		dollarSignText = this.transform.Find ("DollarSignText").GetComponent<Text> ();

		if (Screen.width < 350) {
			text.fontSize = 30;
			dollarSignText.fontSize = 30;
		} else if (Screen.width < 500) {
			text.fontSize = 50;
			dollarSignText.fontSize = 50;
		} else {
			text.fontSize = 75;
			dollarSignText.fontSize = 75;
		}

		UpdateText ();
	}
	
	public void IncreaseCash(long amount) {
		GameData.cash += amount;
		UpdateText ();
	}

	public void DecreaseCash(long amount) {
		GameData.cash -= amount;
		UpdateText ();
	}

	private void UpdateText() {
		text.text = GameData.cash.ToString (GameData.numberFormat, GameData.formatProvider);
	}
}
