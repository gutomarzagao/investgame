﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class TotalInvestedText : MonoBehaviour {

	private Text text;
	private long totalPosition;

	void Start () {
		text = this.GetComponent<Text> ();
	}

	void Update () {
		UpdateText ();
	}

	private void UpdateText () {
		string position = GameData.totalPosition.ToString (GameData.numberFormat, GameData.formatProvider);
		text.text = string.Format ("Total investido <b>${0}</b>", position);
	}

}
