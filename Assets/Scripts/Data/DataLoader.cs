﻿using UnityEngine;
using System;
using System.Collections;

public class DataLoader : MonoBehaviour {

	void Awake () {
		UpdateData ();
	}

	void Update () {
		UpdateData ();
	}

	private void UpdateData () {
		GameData.totalPosition = GameData.investments.GetTotalPosition ();
		UpdateExperienceAndLevel ();
	}

	private void UpdateExperienceAndLevel () {
		long totalExp = GameData.totalPosition + GameData.cash - GameData.InitialCash + (long)GameData.maxInvestExp;
		int level = 1;

		for (long levelExp = GetLevelExperience (level); totalExp >= levelExp; levelExp = GetLevelExperience (++level)) {
			totalExp -= levelExp;
		}

		GameData.experience = totalExp;
		GameData.level = level;
	}

	public static long GetLevelExperience (int level) {
		if (GameData.experienceProgress.ContainsKey (level)) {
			return GameData.experienceProgress [level];
		}

		return GameData.experienceProgressFunction (level);
	}

}
