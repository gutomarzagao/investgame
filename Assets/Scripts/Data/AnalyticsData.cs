﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SA.Analytics.Google;

[Serializable]
public class AnalyticsData : MonoBehaviour {
	
	private static string dataPath;

	private int highestLevel = 1;
	private DateTime firstPlay = DateTime.Now;

	void Awake () {
		dataPath = Application.persistentDataPath + "/analytics.dat";
		LoadData ();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameData.level > highestLevel) {
			highestLevel++;
			MoneyAllocationEvents ();
			DateEvents ();
		}
	}

	private void DateEvents () {
		TimeSpan elapsedTime = DateTime.Now - firstPlay;
		int totalDays = (int)elapsedTime.TotalDays;

		string level = string.Format ("Subiu para o level {0}", highestLevel);
		string day = string.Format ("Dia {0}", totalDays);

		Manager.Client.SendEventHit("Level", level, day);
	}

	private void MoneyAllocationEvents () {
		long total = GameData.totalPosition + GameData.cash;
		SendMoneyEvent ("Disponível", GameData.cash, total);

		foreach (InvestmentType type in Enum.GetValues (typeof(InvestmentType))) {
			string label = GameData.investmentStats [type].name;
			InvestmentPosition position = GameData.investments.GetPosition (type);

			SendMoneyEvent (label, position.totalAmount, total);
		}
	}

	private void SendMoneyEvent (string label, long value, long total) {
		string category = "Investimentos";
		string action = string.Format ("Distribuição no level {0} (%)", highestLevel);

		int allocation = total != 0 ? Convert.ToInt32 (100 * (double)value / (double)total) : 0;
		Manager.Client.SendEventHit(category, action, label, allocation);
	}

	void OnApplicationFocus (bool hasFocus) {
		if (!hasFocus) {
			SaveData ();
		}
	}

	private void SaveData () {
		BinaryFormatter formatter = new BinaryFormatter ();
		FileStream stream = File.Create(dataPath);

		Data data = new Data ();
		data.highestLevel = this.highestLevel;
		data.firstPlay = this.firstPlay;

		formatter.Serialize (stream, data);
		stream.Close ();
	}

	private void LoadData () {
		if (File.Exists (dataPath)) {
			BinaryFormatter formatter = new BinaryFormatter ();
			FileStream stream = File.Open(dataPath, FileMode.Open);

			Data data = formatter.Deserialize (stream) as Data;
			stream.Close ();

			this.highestLevel = data.highestLevel;
			this.firstPlay = data.firstPlay;
		}
	}

	[Serializable]
	private class Data {
		public int highestLevel;
		public DateTime firstPlay;
	}
}
