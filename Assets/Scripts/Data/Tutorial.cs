﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tutorial : MonoBehaviour
{
	public bool EnableTutorial;

	public GameObject welcomePopup;

	public GameObject firstInvestment;
	public ScrollRect scrollPanel;
	public ProductList productList;
	public InvestmentType investmentType;
	private Button plusBtn;
	private Button investBtn;

	public GameObject levelBasics;

	private RectTransform topBackground;
	private RectTransform bottomBackground;
	private RectTransform leftBackground;
	private RectTransform rightBackground;

	// Use this for initialization
	void Start ()
	{
		if (GameData.tutorial && EnableTutorial) {
			this.transform.SetAsLastSibling ();

			topBackground = this.transform.Find ("Background/Top").GetComponent<RectTransform> ();
			bottomBackground = this.transform.Find ("Background/Bottom").GetComponent<RectTransform> ();
			leftBackground = this.transform.Find ("Background/Left").GetComponent<RectTransform> ();
			rightBackground = this.transform.Find ("Background/Right").GetComponent<RectTransform> ();

			scrollPanel.enabled = false;

			GameObject firstProduct = productList.GetInvestment (investmentType);
			plusBtn = firstProduct.transform.Find ("InvestmentInfo/ActionButton").GetComponent<Button> ();
			investBtn = firstProduct.transform.Find ("InvestmentSelection/OkButton").GetComponent<Button> ();
		} else {
			Destroy (gameObject);
		}
	}

	public void SetFirstInvestment ()
	{
		Destroy (welcomePopup);
		firstInvestment.SetActive (true);
		PositionBackground (0.62f, 0.54f, 0.82f, 0.98f);
		plusBtn.onClick.AddListener (() => MakeFirstInvestment ());
	}

	private void MakeFirstInvestment ()
	{
		Destroy (firstInvestment);
		PositionBackground (0.68f, 0.47f, 0f, 1f);
		investBtn.onClick.AddListener (() => LevelBasics ());
	}

	private void LevelBasics ()
	{
		levelBasics.SetActive (true);
		PositionBackground (1f, 0.91f, 0.67f, 1f);
	}

	public void FinishTutorial ()
	{
		scrollPanel.enabled = true;
		Destroy (gameObject);
		GameData.tutorial = false;
	}

	private void PositionBackground (float top, float bottom, float left, float right)
	{
		topBackground.anchorMin = new Vector2 (0, top);
		bottomBackground.anchorMax = new Vector2 (1, bottom);

		leftBackground.anchorMin = new Vector2 (0, bottom);
		leftBackground.anchorMax = new Vector2 (left, top);

		rightBackground.anchorMin = new Vector2 (right, bottom);
		rightBackground.anchorMax = new Vector2 (1, top);
	}
}
