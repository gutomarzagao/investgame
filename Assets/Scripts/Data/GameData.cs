﻿using UnityEngine;
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameData : MonoBehaviour {

	private static GameData instance;
	private static string dataPath;
	public static readonly string numberFormat = "N0";
	public static readonly IFormatProvider formatProvider = CultureInfo.GetCultureInfo("pt-BR");

	public const long InitialCash = 1000;

	// These variables define the pace of game time relative to real world time
	private static readonly TimeSpan worldYear = TimeSpan.FromMinutes (1);
	private static readonly TimeSpan gameYear = TimeSpan.FromSeconds (1);
	public static readonly double timeRate = (double) worldYear.Ticks / (double) gameYear.Ticks;
	public static readonly TimeSpan plantationTime = TimeSpan.FromSeconds (30);
	public static readonly long plantationEarnings = 1000;

	public static readonly Dictionary<InvestmentType, InvestmentParam> investmentStats = new Dictionary<InvestmentType, InvestmentParam> () {
		{ InvestmentType.Poupanca, new InvestmentParam (1, "Poupança", 8.33M, 0, TimeSpan.Zero) },
		{ InvestmentType.TesouroDireto, new InvestmentParam (3, "Tesouro Direto", 12.05M, 30, TimeSpan.Zero) },
		{ InvestmentType.CDB, new InvestmentParam (7, "CDB", 12.7M, 1000, TimeSpan.Zero) },
		{ InvestmentType.LCI, new InvestmentParam (15, "LCI", 13.11M, 10000, TimeSpan.FromDays(180), "6 meses") },
		{ InvestmentType.Debenture, new InvestmentParam (20, "Debênture", 14.02M, 10000, TimeSpan.FromDays(1800), "5 anos") },
		{ InvestmentType.LetraDeCambio, new InvestmentParam (26, "Letra de Câmbio", 14.41M, 10000, TimeSpan.FromDays(1080), "3 anos") }
	};

	public static readonly double InvestExperienceFactor = 1 / (double)investmentStats [InvestmentType.Poupanca].yearProfit;

	public static readonly Dictionary<int, long> experienceProgress = new Dictionary<int, long> () {
		{ 1, 100 }, { 2, 1000 }, { 3, 2000 }, { 4, 3000 }, { 5, 5000 }, { 6, 10000 }, { 7, 15000 }, { 8, 22000 }, { 9, 30000 }, { 10, 55000 },
		{ 11, 75000 }, { 12, 115000 }, { 13, 190000 }, { 14, 285000 }, { 15, 515000 }, { 16, 700000 }, { 17, 980000 }, { 18, 1300000 }, { 19, 1700000 }
	};

	public static long experienceProgressFunction(int level) {
		if (level <= 20) {
			return 2000000;
		}

		return (long)Math.Pow(experienceProgressFunction(level - 1), 1.02);
	}

	// Persisted data
	public static long cash = InitialCash;
	public static Investments investments = new Investments ();
	public static bool tutorial = true;
	public static DateTime startingDate = DateTime.Now;
	public static double investExp = 0;
	public static double maxInvestExp = 0;
	public static DateTime?[] plantation;

	// Calculated data
	public static long totalPosition;
	public static long experience;
	public static int level;

	void Awake () {
		if (instance == null) {
			dataPath = Application.persistentDataPath + "/data.dat";
			DontDestroyOnLoad (gameObject);
			instance = this;
			LoadData ();
		} else if (instance != this) {
			Destroy (gameObject);
		}
	}

	void OnApplicationFocus (bool hasFocus) {
		if (!hasFocus) {
			SaveData ();
		}
    }

	private void SaveData () {
		BinaryFormatter formatter = new BinaryFormatter ();
		FileStream stream = File.Create(dataPath);

		Data data = new Data ();
		data.cash = GameData.cash;
		data.investments = GameData.investments;
		data.tutorial = GameData.tutorial;
		data.startingDate = GameData.startingDate;
		data.investExp = GameData.investExp;
		data.maxInvestExp = GameData.maxInvestExp;
		data.plantation = GameData.plantation;

		formatter.Serialize (stream, data);
		stream.Close ();
	}

	private void LoadData () {
		if (File.Exists (dataPath)) {
			BinaryFormatter formatter = new BinaryFormatter ();
			FileStream stream = File.Open(dataPath, FileMode.Open);

			Data data = formatter.Deserialize (stream) as Data;
			stream.Close ();

			GameData.cash = data.cash;
			GameData.investments = data.investments;
			GameData.tutorial = data.tutorial;
			GameData.startingDate = data.startingDate ?? DateTime.Now;
			GameData.investExp = data.investExp;
			GameData.maxInvestExp = data.maxInvestExp;
			GameData.plantation = data.plantation;
		}
	}

	[Serializable]
	private class Data {
		public long cash;
		public Investments investments;

		[DefaultValue (false)]
		public bool tutorial;

		public DateTime? startingDate;

		public double investExp;
		public double maxInvestExp;

		public DateTime?[] plantation;
	}

}

public class InvestmentParam {
	public string name;
	public int minLevel;
	public decimal yearProfit;
	public long minAmount;
	public TimeSpan minTimeSpan;
	public string redeemString;

	public InvestmentParam (int minLevel, string name, decimal yearProfit, long minAmount, TimeSpan minTimeSpan, string redeemString = "") {
		this.name = name;
		this.minLevel = minLevel;
		this.yearProfit = yearProfit;
		this.minAmount = minAmount;
		this.redeemString = redeemString;

		// Convert to game time
		this.minTimeSpan = TimeSpan.FromTicks ((long) (minTimeSpan.Ticks / GameData.timeRate));
	}
}

public enum InvestmentType {
	Poupanca, TesouroDireto, CDB, LCI, Debenture, LetraDeCambio
}
