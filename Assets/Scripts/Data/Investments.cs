﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Investments {
	
	private Dictionary<InvestmentType, LinkedList<InvestmentSupply>> supplies = new Dictionary<InvestmentType, LinkedList<InvestmentSupply>> ();

	public bool Invest (InvestmentType type, long amount) {
		if (amount < GameData.investmentStats [type].minAmount) {
			return false;
		}

		if (!supplies.ContainsKey (type)) {
			supplies.Add (type, new LinkedList<InvestmentSupply> ());
		}

		DateTime now = DateTime.Now;
		supplies [type].AddLast (new InvestmentSupply (now, now, amount));

		return true;
	}

	public long Redeem (InvestmentType type, long amount = long.MaxValue) {
		if (!supplies.ContainsKey (type)) {
			return 0;
		}

		double amountRedeemed = 0;
		double targetAmount = amount;

		DateTime now = DateTime.Now;
		double interestRate = (double) (GameData.investmentStats [type].yearProfit / 100M);
		LinkedListNode<InvestmentSupply> node = supplies [type].First;

		while (targetAmount > 0 && node != null) {
			InvestmentSupply supply = node.Value;
			LinkedListNode<InvestmentSupply> nextNode = node.Next; 

			if (now - supply.initialDateTime < GameData.investmentStats [type].minTimeSpan) {
				break;
			}

			double supplyAmount = CalculateInterests (supply, interestRate, now);

			if (targetAmount < supplyAmount) {
				amountRedeemed += targetAmount;
				supply.balanceDateTime = now;
				supply.balanceAmount = supplyAmount - targetAmount;
				break;
			} else {
				targetAmount -= supplyAmount;
				amountRedeemed += supplyAmount;

				supplies [type].Remove (node);
			}

			node = nextNode;
		}

		return (long) amountRedeemed;
	}

	public bool HasType (InvestmentType type) {
		if (supplies.ContainsKey (type) && supplies [type].Count != 0) {
			return true;
		}

		return false;
	}

	public InvestmentPosition GetPosition (InvestmentType type) {
		if (!supplies.ContainsKey (type)) {
			return new InvestmentPosition();
		}

		double totalAmount = 0;
		double availableAmount = 0;
		double interestRate = (double) (GameData.investmentStats [type].yearProfit / 100M);
		DateTime now = DateTime.Now;

		foreach (InvestmentSupply supply in supplies[type]) {
			double amount = CalculateInterests (supply, interestRate, now);
			totalAmount += amount;

			if (now - supply.initialDateTime >= GameData.investmentStats [type].minTimeSpan) {
				availableAmount += amount;
			}
		}

		InvestmentPosition position = new InvestmentPosition ();
		position.totalAmount = (long) totalAmount;
		position.availableAmount = (long) availableAmount;

		return position;
	}

	public long GetTotalPosition () {
		long totalPosition = 0;

		foreach (InvestmentType type in Enum.GetValues (typeof(InvestmentType))) {
			totalPosition += GetPosition(type).totalAmount;
		}

		return totalPosition;
	}

	private double CalculateInterests(InvestmentSupply supply, double interestRate, DateTime now) {
		TimeSpan elapsedTime = now - supply.balanceDateTime;
		double timeInWorldYears = elapsedTime.TotalDays * GameData.timeRate / 360;
		return supply.balanceAmount * Math.Pow (1 + interestRate, timeInWorldYears);
	}
}

[Serializable]
public class InvestmentSupply {
	public DateTime initialDateTime;
	public DateTime balanceDateTime;
	public double balanceAmount;

	public InvestmentSupply (DateTime initialDateTime, DateTime balanceDateTime, double balanceAmount) {
		this.initialDateTime = initialDateTime;
		this.balanceDateTime = balanceDateTime;
		this.balanceAmount = balanceAmount;
	}
}

public class InvestmentPosition {
	public long totalAmount;
	public long availableAmount;
}
